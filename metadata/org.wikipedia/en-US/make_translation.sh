#!/bin/bash
# Author: Jean-Baptiste Holcroft (jibecfed) <jean-baptiste@holcroft.fr>
# This file produce a po4a config file and run po4a
# po4a update all pot, po and publish content
# po4a: http://po4a.alioth.debian.org/
# po4a general help: http://po4a.alioth.debian.org/man/man7/po4a.7.php
# po4a configuration file: http://po4a.alioth.debian.org/man/man1/po4a.1.php

# What directory is to scan
dir="." # no trailing slash
PO4ACONF=./po4a.cfg

# [po4a_langs] Enable fr lang code
# [po4a_paths] Make a global pot named "wikipedia" file for all files found,
#    store it in a 'po' directory and create a '$lang' sub-dir for each language
#    Note, if we want one po file per source file: s/wikipedia/$master/
# [options] publish a translated file, whatever the translation level
cat >$PO4ACONF <<'EOT'
[po4a_langs] fr
[po4a_paths] pot/wikipedia.pot $lang:po/$lang.po
[options] opt:"-k 0"

EOT

# for every txt file we find, pass it to po4a
for file in $( find -L $dir -name "*.txt" ); do
    # update list of file to translate
    echo "[type: text] $file \$lang:publish/\$lang/$file"
    echo "[type: text] $file \$lang:publish/\$lang/$file" >> $PO4ACONF
done

# generate po files and generate translated content
# one run is sufficient for generating pot, update po files and publish content
po4a $PO4ACONF \
        --package-name "I love package names" \
        --package-version `date +%Y%m%d`\
        --msgid-bugs-address translation@fdroid.org \
        --master-charset utf8 \
        --verbose
